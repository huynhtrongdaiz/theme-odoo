{
    'name': 'Van Phuoc Thinh',
    'description': 'A description for your theme.',
    'version': '1.0',
    'author': 'Kimdai huynh',
    'category': 'Theme/Creative',
    'depends': ['website'],
    'data': [
        'views/header.xml',
        'views/layouts.xml',
        'views/templates.xml',
        'views/assets.xml',
        'views/snippets/card.xml',
        'views/snippets/fillter.xml'
    ],
}